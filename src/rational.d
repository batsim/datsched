import deimos.gmp.gmp;
import deimos.gmp.rational;
import core.stdc.config : c_long;

import std.string, std.stdio, std.conv;
import std.experimental.logger;

enum DefaultBase = 10;

static import core.stdc.stdio;

struct Rational
{
    mpq_t value;
    bool initialized = false;

    void initialize()
    in { assert(!initialized, "Multiple initialization"); }
    out { assert(initialized); }
    body { mpq_init(&value); initialized = true; }

    this(in c_long num, in c_long denom)
    in { assert(denom != 0, "Invalid fraction: division by zero"); }
    body {
        initialize;
        mpq_set_si(&value, num, denom);
    }
    this(in double v)
    body {
        initialize;
        mpq_set_d(&value, v);
    }
    this(this)
    in { assert(initialized, "Copying non-initialized Rational"); }
    body {
        mpq_t tmp;
        mpq_init(&tmp);
        mpq_set(&tmp, &value);
        value = tmp;
    }

    ~this()
    body {
        if (initialized)
        {
            mpq_clear(&value);
            initialized = false;
        }
    }

    ref Rational opAssign()(auto ref const Rational v)
    in {
        assert(initialized, "Assigning to non-initialized Rational");
        assert(v.initialized, "Assigning from non-initialized Rational");
    }
    out(result) { assert(mpq_cmp(&result.value, &v.value) == 0); }
    body {
        mpq_set(&value, &v.value);
        return this;
    }

    ref Rational opAssign()(double v)
    in { assert(initialized, "Assigning non-initialized Rational"); }
    body {
        mpq_set_d(&value, v);
        return this;
    }

    ref Rational opOpAssign(string op)(auto ref const Rational v)
    in {
        assert(initialized, "Assigning to non-initialized Rational");
        assert(v.initialized, "Assigning from non-initialized Rational");
    }
    body {
        static if (op == "+")
            mpq_add(&value, &value, &v.value);
        else static if (op == "-")
            mpq_sub(&value, &value, &v.value);
        else static if (op == "*")
            mpq_mul(&value, &value, &v.value);
        else static if (op == "/")
            mpq_div(&value, &value, &v.value);
        else static assert("Operator " ~ op ~ " is unimplemented");

        return this;
    }
    Rational opOpAssign(string op)(auto ref const int i)
    in { assert(initialized, "Using non-initialized Rational"); }
    body {
        return this.opOpAssign!op(Rational(i,1));
    }

    Rational opUnary(string op)() const
        if (op == "-")
        in { assert(initialized, "Using non-initialized Rational"); }
        body {
            Rational ret;
            ret.initialize;
            mpq_neg(&ret.value, &value);
            return ret;
        }

    Rational opBinary(string op)(auto ref const Rational v) const
    in { assert(initialized, "Using non-initialized Rational");
        assert(v.initialized, "Using non-initialized Rational"); }
    body {
        Rational ret;
        ret.initialize;

        static if (op == "+")
            mpq_add(&ret.value, &value, &v.value);
        else static if (op == "-")
            mpq_sub(&ret.value, &value, &v.value);
        else static if (op == "*")
            mpq_mul(&ret.value, &value, &v.value);
        else static if (op == "/")
            mpq_div(&ret.value, &value, &v.value);
        else static assert("Operator " ~ op ~ " is unimplemented");

        return ret;
    }
    Rational opBinary(string op)(auto ref const int i) const
    body { return this.opBinary!op(Rational(i,1)); }
    Rational opBinaryRight(string op)(auto ref const int i) const
    body { return this.opBinary!op(Rational(i,1)); }

    Rational abs() const
    in { assert(initialized, "Using non-initialized Rational"); }
    body {
        Rational ret;
        mpq_abs(&ret.value, &value);
        return ret;
    }

    double opCast(T : double)() const
    in { assert(initialized, "Using non-initialized Rational"); }
    body { return mpq_get_d(&value); }

    string opCast(T : string)() const
    in { assert(initialized, "Using non-initialized Rational"); }
    body { return toString(mpq_get_str(null, DefaultBase, &value)); }

    bool opEquals(in c_long v) const
    in { assert(initialized, "Using non-initialized Rational"); }
    body { return _mpq_cmp_si(&value, v, 1) == 0; }

    bool opEquals()(auto ref const Rational v) const
    in { assert(initialized, "Using non-initialized Rational");
        assert(v.initialized, "Using non-initialized Rational"); }
    body { return mpq_cmp(&value, &v.value) == 0; }

    int opCmp(in c_long v) const
    in { assert(initialized, "Using non-initialized Rational"); }
    body { return _mpq_cmp_si(&value, v, 1); }

    int opCmp()(auto ref const Rational v) const
    in { assert(initialized, "Using non-initialized Rational");
        assert(v.initialized, "Using non-initialized Rational"); }
    body { return mpq_cmp(&value, &v.value); }

    void print() const
    in { assert(initialized, "Using non-initialized Rational"); }
    body { mpq_out_str(core.stdc.stdio.stdout, DefaultBase, &value); }

    void println() const
    in { assert(initialized, "Using non-initialized Rational"); }
    body {
        print();
        core.stdc.stdio.printf("\n");
    }
    string toString() const
    in { assert(initialized, "Using non-initialized Rational"); }
    body { return to!string(cast(double)this); }
}

unittest
{
    // Copies, etc.
    Rational a = Rational(0,1);
    Rational b = Rational(1,1);
    assert(a == 0);
    assert(b == 1);

    a = b;
    assert(a == 1);
    assert(a == b);

    b = 2;
    assert(a == 1);
    assert(b == 2);

    Rational c = b;
    assert(c == b);
    c = 36;
    assert(b == 2);
    assert(c == 36);
}

unittest
{
    Rational a;
    a.initialize;

    Rational b = Rational(0,1);
    Rational c = Rational(1,1);
    Rational d = 2.5;
    Rational a2 = a;
    Rational b2 = b;
    Rational d2 = d;

    assert(a == 0, "Initialized Rational should have a value of 0");
    assert(b == 0);
    assert(a == b);

    assert(a == a2);
    assert(b == b2);
    assert(d == d2);
    assert(d == Rational(5,2));
    assert(Rational(42,1) == Rational(84,2));

    // op binary
    assert(a+1 == c);

    assert(a * b == 0);
    assert(a * c == 0);
    assert(c * c == c);
    assert(c * d == d);
    assert(d / c == d);

    // opopassign
    a = Rational(1,1);
    assert(a == 1);

    a += 5;
    assert(a == 6);

    a /= 2;
    assert(a == 3);

    a *= Rational(1,2);
    assert(a == Rational(3,2));

    a -= Rational(1,2);
    assert(a == 1);

    // op unary -
    a = Rational(42,1);
    assert(-a == -42);
    assert(-a == a * -1);
    assert(-a == -1 * a);
}
