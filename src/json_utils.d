import std.json, std.conv, std.format, std.algorithm, std.exception;

bool get_bool(in JSONValue v)
{
    if (v.type == JSON_TYPE.TRUE)
        return true;
    else if (v.type == JSON_TYPE.FALSE)
        return false;
    else if (v.type == JSON_TYPE.INTEGER)
    {
        enforce([0,1].canFind(v.integer),
               format!"Cannot deduce boolean value from integer %d"(v.integer));
        return to!bool(v.integer);
    }
    else if (v.type == JSON_TYPE.UINTEGER)
    {
        enforce([0,1].canFind(v.uinteger),
               format!"Cannot deduce boolean value from uinteger %d"(v.uinteger));
        return to!bool(v.uinteger);
    }
    else
    {
        enforce(0, "Cannot read bool value from JSONValue " ~ v.toString);
        return bool.init;
    }
}
unittest
{
    auto json = parseJSON(`{"a":true,
                            "b":false,
                            "c":0,
                            "d":1,
                            "e":2,
                            "f":42.51}`);

    assert(json["a"].get_bool == true);
    assert(json["b"].get_bool == false);
    assert(json["c"].get_bool == false);
    assert(json["d"].get_bool == true);

    auto e = collectException(json["e"].get_bool);
    assert(e, "Should NOT be able to retrieve boolean value from JSON integer 2");

    e = collectException(json["f"].get_bool);
    assert(e, "Should NOT be able to retrieve boolean value from JSON double 42.51");
}


int get_int(in JSONValue v)
{
    if (v.type == JSON_TYPE.INTEGER)
        return to!int(v.integer);
    else if (v.type == JSON_TYPE.UINTEGER)
        return to!int(v.uinteger);
    else
    {
        enforce(0, "Cannot read int value from JSONValue " ~ v.toString);
        return int.init;
    }
}
unittest
{
    auto json = parseJSON(`{"a":-10,
                            "b":37,
                            "c":42000,
                            "d":123456789,
                            "e":true,
                            "f":42.51}`);

    assert(json["a"].get_int == -10);
    assert(json["b"].get_int == 37);
    assert(json["c"].get_int == 42000);
    assert(json["d"].get_int == 123456789);

    auto e = collectException(json["e"].get_int);
    assert(e, "Should NOT be able to retrieve integer value from JSON bool");

    e = collectException(json["f"].get_int);
    assert(e, "Should NOT be able to retrieve integer value from JSON double 42.51");
}


double get_double(in JSONValue v)
{
    if (v.type == JSON_TYPE.FLOAT)
        return v.floating;
    else if (v.type == JSON_TYPE.INTEGER)
        return to!double(v.integer);
    else if (v.type == JSON_TYPE.UINTEGER)
        return to!double(v.uinteger);
    else
    {
        enforce(0, "Cannot read double value from JSONValue " ~ v.toString);
        return double.init;
    }
}
unittest
{
    auto json = parseJSON(`{"a":-10,
                            "b":37,
                            "c":42000,
                            "d":123456789,
                            "e":true,
                            "f":42.51}`);

    assert(json["a"].get_double == -10);
    assert(json["b"].get_double == 37);
    assert(json["c"].get_double == 42000);
    assert(json["d"].get_double == 123456789);

    auto e = collectException(json["e"].get_double);
    assert(e, "Should NOT be able to retrieve double value from JSON bool");

    assert(json["f"].get_double == 42.51);
}
