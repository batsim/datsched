import core.thread, core.time;
import std.stdio, std.format, std.json, std.conv;
import zmqd;

import std.experimental.logger;
import interval_set;

class Protocol
{
    // Fields
    Socket socket;
    static immutable buf_size = 1024*1024;
    ubyte[buf_size] buffer;
    JSONValue events = JSONValue(string[].init);
    double date = 0;

    // Invariants
    invariant
    {
        assert(events.type == JSON_TYPE.ARRAY, "Invalid events JSON type");
    }

    // Constructor
    this()
    {
        socket = Socket(SocketType.rep);
    }

    // Classical methods
    void bind(in string bind_target = "tcp://*:28000")
    {
        info("Binding socket to " ~ bind_target);
        socket.bind(bind_target);
    }


    void recv(out double received_date, out JSONValue received_events)
    out
    {
        assert(received_date == date, "Date inconsistency");
        assert(received_events.type == JSON_TYPE.ARRAY,
               "Non-array received events");
    }
    body
    {
        // Waits a message on the socket
        immutable size = socket.receive(buffer);
        assert(size < buf_size);

        // Gets a string from the byte buffer
        string msg = buffer[0 .. size].idup.asString();
        info("Received message ", msg);

        // Parse the string as JSON
        JSONValue json_msg = parseJSON(msg);
        assert(json_msg.type == JSON_TYPE.OBJECT,
               "Non-object received on socket");

        received_date = json_msg["now"].floating;
        assert(received_date >= date, "Message received from the past");
        date = received_date;

        received_events = json_msg["events"];
    }


    void send(in double send_date)
    in
    {
        assert(send_date >= this.date,
               format!"Sending in the past: %g >= %g is false"
                      (send_date, this.date));
    }
    out
    {
        assert(events.array.length == 0, "Non-empty events array after send");
        assert(send_date == date,
               format!"Date inconsistency: %g == %g is false"(send_date, date));
    }
    body
    {
        // Create the "main" JSON object with some fields
        JSONValue msg_json = ["now": send_date];
        msg_json.object["events"] = events;

        // Sends the message on the socket
        string msg_str = msg_json.toString;
        info("Sending ", msg_str);
        socket.send(msg_str);

        // Clears the events array
        events = JSONValue(string[].init);
        date = send_date;
    }


    void reject_job(in double date,
                    in string job_id)
    in
    {
        assert(date >= this.date,
               format!"Event in the past: %g >= %g is false"(date, this.date));
    }
    out
    {
        assert(date == this.date,
               format!"Date inconsistency: %g == %g is false"(date, this.date));
    }
    body
    {
        info(format!"REJECT_JOB %s"(job_id));
        JSONValue event_data = ["job_id": job_id];

        JSONValue event = ["timestamp": date];
        event.object["type"] = "REJECT_JOB";
        event.object["data"] = event_data;

        events.array ~= event;
        this.date = date;
    }

    void execute_job(in double date,
                     in string job_id,
                     in IntervalSet resources)
    in
    {
        assert(date >= this.date,
               format!"Event in the past: %g >= %g is false"(date, this.date));
    }
    out
    {
        assert(date == this.date,
               format!"Date inconsistency: %g == %g is false"(date, this.date));
    }
    body
    {
        info(format!"EXECUTE_JOB %s on %s"(job_id, resources.to_string_protocol));
        JSONValue event_data = ["job_id": job_id,
                                "alloc": resources.to_string_protocol];

        JSONValue event = ["timestamp": date];
        event.object["type"] = "EXECUTE_JOB";
        event.object["data"] = event_data;

        events.array ~= event;
        this.date = date;
    }
}
