Another Batsim scheduler. This one is implemented in D!

## Build, run...
This project uses the dub package manager.

### Build
``dub build``

### Run
``dub run ``

The program accept different options. ``-h`` or ``--help`` should display them.

However, most parameters are given through a JSON configuration file.
The default configuration file is displayed with ``--show-default-config``.

``` bash
datsched --show-default-config | python -m json.tool
```

``` json
{
    "algo": "fifo",
    "queue_order": "fcfs",
    "socket_bindpoint": "tcp://*:28000"
}
```

### Run the unit tests
``dub test``
