import std.json;

import interval_set;
import job;
import proto;

class Algorithm
{
    this()
    {
        clean_recent_states();
    }

    enum SimuStatus : string
    {
        NOT_STARTED = "not_started",
        STARTED = "started",
        FINISHED = "finished"
    }

    private
    {
        Protocol _protocol;
        Workload _workload;

        SimuStatus _status = SimuStatus.NOT_STARTED;
        JSONValue _batsim_config;
        int _nb_resources;

        string[] _newly_submitted_jobs;
        string[] _newly_completed_jobs;
        string[] _newly_killed_jobs;
        IntervalSet[string] _newly_state_changed_resources;
        bool _requested_call_received;
    }

    @property final Workload workload() { return _workload; }
    @property final void workload(Workload w) { _workload = w; }
    @property final Protocol protocol() { return _protocol; }
    @property final void protocol(Protocol p) { _protocol = p; }

    @property final SimuStatus status() { return _status; }
    @property final int nb_resources()
    in { assert(status != SimuStatus.NOT_STARTED,
        "Cannot access nb_resources before simulation start"); }
    body { return _nb_resources; }
    @property final JSONValue batsim_config()
    in { assert(status != SimuStatus.NOT_STARTED,
        "Cannot access Batsim config before simulation start"); }
    body { return _batsim_config; }

    @property final string[] newly_submitted_jobs() { return _newly_submitted_jobs; }
    @property final string[] newly_completed_jobs() { return _newly_completed_jobs; }
    @property final string[] newly_killed_jobs() { return _newly_killed_jobs; }
    @property final IntervalSet[string] newly_state_changed_resources()
        { return _newly_state_changed_resources; }
    @property final bool requested_call_received() { return _requested_call_received; }

    final void clean_recent_states()
    {
        _newly_submitted_jobs.length = 0;
        _newly_completed_jobs.length = 0;
        _newly_killed_jobs.length = 0;
        _newly_state_changed_resources.clear();
        _requested_call_received = false;
    }


    void on_simulation_start(in double date,
                             in int nb_resources,
                             in JSONValue batsim_config)
    in { assert(status == SimuStatus.NOT_STARTED);
         assert(!(workload is null), "Workload unset");
         assert(!(protocol is null), "Protocol unset"); }
    out { assert(status == SimuStatus.STARTED);
          assert(nb_resources == _nb_resources); }
    body
    {
        _status = SimuStatus.STARTED;
        _nb_resources = nb_resources;
    }


    void on_simulation_end(in double date)
    in { assert(status == SimuStatus.STARTED); }
    out { assert(status == SimuStatus.FINISHED); }
    body { _status = SimuStatus.FINISHED; }


    void on_job_submitted(in double date,
                          in string job)
    body { _newly_submitted_jobs ~= [job]; }


    void on_job_completed(in double date,
                          in string job,
                          in string status)
    body { _newly_completed_jobs ~= [job]; }


    void on_job_killed(in double date,
                       in string[] jobs)
    body { _newly_killed_jobs ~= jobs; }


    void on_resource_state_changed(in double date,
                                   in IntervalSet resources,
                                   in string new_state)
    body
    {
        if (new_state in _newly_state_changed_resources)
            _newly_state_changed_resources[new_state] |= resources;
        else
            _newly_state_changed_resources[new_state] = IntervalSet(resources);
    }

    void on_requested_call(in double date)
    out { assert(requested_call_received == true); }
    body { _requested_call_received = true; }


    void on_query_reply_energy(in double consumed_energy)
    body { assert(0, "Not implemented!"); }

    void make_decisions(in double date)
    body { assert(0, "Not implemented!"); }
}

unittest
{
    auto algo = new Algorithm;

    auto proto = new Protocol;
    algo.protocol = proto;

    auto w = new Workload;
    algo.workload = w;

    algo.on_simulation_start(0, 10, JSONValue());
}
