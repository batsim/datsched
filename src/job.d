import std.json;
import std.conv;

import rational;
import json_utils;

class Job
{
    string id;
    int nb_requested_resources;
    Rational walltime;
    Rational submission_time;

    this()
    {
        walltime.initialize;
        submission_time.initialize;
    }

    static Job from_json(in JSONValue json)
    {
        Job j = new Job;

        j.id = json["id"].str;
        j.nb_requested_resources = json["res"].get_int;
        j.walltime = json["walltime"].get_double;

        return j;
    }

    override string toString() const
    {
        return id;
    }
}

class Workload
{
    private
    {
        Job[string] _jobs;
    }

    ref Job opIndex(in string job_id)
    in { assert(job_id in _jobs, "Unknown job " ~ job_id); }
    body { return _jobs[job_id]; }

    void add_job(Job j)
    in { assert(!(j.id in _jobs), "Job " ~ j.id ~ " is already in the workload"); }
    body { _jobs[j.id] = j; }
}
