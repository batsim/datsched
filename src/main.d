import std.stdio, std.format, std.json, std.conv, std.getopt, std.range;
import std.exception, std.file;
import std.experimental.logger;

import zmqd;

import job;
import algorithm;
import proto;
import interval_set;
import fifo;
import json_utils;

void main(string[] args)
{
    string config_file;
    bool show_default_config = false;

    auto helpInformation = getopt(
        args,
        "config-file", "Sets configuration file", &config_file,
        "show-default-config", "Displays default configuration", &show_default_config
    );

    if (helpInformation.helpWanted)
    {
        defaultGetoptPrinter("A Batsim scheduler in D.",
                             helpInformation.options);
        return;
    }

    // todo: verbosity in config

    immutable string default_config = `{"algo": "fifo",
                                        "queue_order": "fcfs",
                                        "socket_bindpoint": "tcp://*:28000"
                                        }`;

    immutable JSONValue default_config_json = default_config.parseJSON;

    if (show_default_config)
    {
        writeln(default_config_json.toString);
        return;
    }

    JSONValue config_json = default_config_json;

    if (!config_file.empty)
    {
        enforce(exists(config_file),
                format!"Configuration file '%s' does not exist."(config_file));
        enforce(isFile(config_file),
                format!"Configuration file '%s' is not a file."(config_file));

        try
        {
            string file_content = readText(config_file);
            JSONValue content_json = file_content.parseJSON;

            enforce(content_json.type == JSON_TYPE.OBJECT, "Not a JSON object");
            enforce(0, "Reading config file is not implemented yet");
        }
        catch (Exception e)
        {
            writefln("Cannot read config file '%s': %s", config_file, e.msg);
            return;
        }
    }

    Protocol proto = new Protocol;
    proto.bind(config_json["socket_bindpoint"].str);

    Algorithm algo = new FIFO;
    // todo: algo options (in config file)

    simulation_loop(proto, algo);
}


void simulation_loop(Protocol proto, Algorithm algo)
{
    auto workload = new Workload;
    algo.workload = workload;
    algo.protocol = proto;

    bool simulation_ended = false;

    while (!simulation_ended)
    {
        double msg_date;
        JSONValue received_events;
        proto.recv(msg_date, received_events);

        algo.clean_recent_states;

        foreach(event; received_events.array)
        {
            double date = event["timestamp"].get_double;
            string type = event["type"].str;
            JSONValue data = event["data"];

            if (type == "SIMULATION_BEGINS")
            {
                int nb_res = data["nb_resources"].get_int;
                JSONValue config = data["config"];
                assert(!config["redis"]["enabled"].get_bool,
                       "Redis should not be enabled!");
                algo.on_simulation_start(date, nb_res, config);
            }
            else if (type == "SIMULATION_ENDS")
            {
                algo.on_simulation_end(date);
                simulation_ended = true;
            }
            else if (type == "JOB_SUBMITTED")
            {
                string job_id = data["job_id"].str;
                Job job = Job.from_json(data["job"]);
                job.id = job_id;
                job.submission_time = date;

                workload.add_job(job);
                algo.on_job_submitted(date, job_id);
            }
            else if (type == "JOB_COMPLETED")
            {
                string job_id = data["job_id"].str;
                string status = data["status"].str;
                algo.on_job_completed(date, job_id, status);
            }
            else assert(0, "Unhandled event type " ~ type);
        }

        algo.make_decisions(msg_date);
        proto.send(msg_date);
    }
}
