import std.json, std.format, std.algorithm, std.conv;

import std.experimental.logger;

import interval_set;
import algorithm;
import job;

class FIFO : Algorithm
{
    private
    {
        Job[] _queue;
        IntervalSet[string] _allocations;
        IntervalSet _available_resources;
    }

    override void on_simulation_start(in double date,
                                      in int nb_resources,
                                      in JSONValue batsim_config)
    {
        Algorithm.on_simulation_start(date, nb_resources, batsim_config);
        _available_resources = IntervalSet(0, nb_resources - 1);
    }

    override void make_decisions(in double date)
    {
        assert(!requested_call_received, "Inconsistency");
        assert(newly_state_changed_resources.length == 0, "Inconsistency");
        assert(newly_killed_jobs.length == 0, "Inconsistency");

        foreach(job_id; newly_submitted_jobs)
        {
            Job job = workload[job_id];

            if (job.nb_requested_resources > nb_resources)
                protocol.reject_job(date, job_id);
            else
                _queue ~= job;
        }

        foreach(job_id; newly_completed_jobs)
        {
            assert(job_id in _allocations,
                   format!"Non-allocated job %s completed"(job_id));

            _available_resources |= _allocations[job_id];
            _allocations.remove(job_id);
        }

        trace("queue: ", _queue);
        trace("allocations: ", _allocations);

        auto nb_available = _available_resources.nb_elements;
        bool[string] newly_allocated_jobs;

        foreach(i, job; _queue)
        {
            if (job.nb_requested_resources <= nb_available)
            {
                auto res = _available_resources.left(job.nb_requested_resources);

                nb_available -= job.nb_requested_resources;
                _available_resources -= res;
                _allocations[job.id] = res;

                protocol.execute_job(date, job.id, res);
                newly_allocated_jobs[job.id] = true;
            }
            else
                break;
        }

        if (newly_allocated_jobs.length > 0)
        {
            _queue.remove!(a => a.id in newly_allocated_jobs);
            _queue.length -= newly_allocated_jobs.length;
        }
    }
}
